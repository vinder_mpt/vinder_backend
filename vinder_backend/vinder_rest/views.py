from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView, View
from .models import Movie, VinderUser, Room, RoomUser, MovieGenre
from .serializers import (
    MovieSerializer,
    UserSerializer,
)
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import User
from hashlib import sha256
from datetime import datetime
from rest_framework.response import Response
from django.http.response import JsonResponse
from django.db.models import Q
import functools
import collections


class MovieView(RetrieveAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer


class UserAPIView(RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get_object(self):
        return self.request.user


class AnonymousLoginView(View):
    def get(self, request):
        now_ts = str(datetime.now().timestamp())
        tmp_username = sha256(bytes(now_ts, encoding="utf-8")).hexdigest()[:8]
        tmp_user = User(username=tmp_username)
        tmp_user.set_unusable_password()
        tmp_user.save()
        tmp_vinder_user = VinderUser(user=tmp_user, is_anonymous=True)
        tmp_vinder_user.save()
        refresh = RefreshToken.for_user(tmp_user)
        data = {"refresh": str(refresh), "access": str(refresh.access_token)}
        return JsonResponse(data=data)


class CreateRoomView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        user = request.user
        genres = request.data["genres"]

        vinder_user = VinderUser.objects.get(user=user)
        new_room = Room()
        new_room_id = Room.generate_room_id()
        new_room.room_id = new_room_id
        new_room.room_owner = vinder_user
        new_room.started = False
        new_room.done = False
        new_room.save()

        room_owner_room_user = RoomUser(room=new_room, vinder_user=vinder_user)
        room_owner_room_user.save()
        for genre_name in genres:
            genre = MovieGenre.objects.get(genre_name=genre_name)
            room_owner_room_user.chosen_genres.add(genre)

        room_owner_room_user.save()
        data = {"newRoomId": new_room_id}
        return Response(data=data)


class JoinRoomView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        room_id = request.data["roomId"]
        room = Room.objects.safe_get(room_id=room_id)
        genres = request.data.get("genres")

        if room is None:
            response_data = {"success": 0, "msg": "No such room"}
            return Response(data=response_data)

        if room.started:
            response_data = {"success": 0, "msg": "Room already started"}
            return Response(data=response_data)

        if room.done:
            response_data = {"success": 0, "msg": "Room is already closed"}
            return Response(data=response_data)

        room_users_for_this_room = RoomUser.objects.filter(room__room_id=room_id)
        vinder_user = VinderUser.objects.get(user=request.user)
        for room_user in room_users_for_this_room:
            if vinder_user == room_user.vinder_user:
                response_data = {"success": 0, "msg": "Already in the room"}
                return Response(data=response_data)

        new_room_user = RoomUser(room=room, vinder_user=vinder_user)
        new_room_user.save()

        genres_to_add = (
            [MovieGenre.objects.get(genre_name=genre_name) for genre_name in genres]
            if genres
            else [genre for genre in MovieGenre.objects.all()]
        )

        for genre in genres_to_add:
            new_room_user.chosen_genres.add(genre)

        new_room_user.save()
        response_data = {"success": 1}
        return Response(data=response_data)


class ModifyRoomStateView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def create_genres_map(room_users):
        all_chosen_genres = [
            user_genre.genre_name
            for room_user in room_users
            for user_genre in room_user.chosen_genres.all()
        ]

        return collections.Counter(all_chosen_genres)

    @staticmethod
    def get_valid_genres_names(genres_map, num_users):
        valid_genres = [key for key in genres_map if genres_map[key] == num_users]
        if len(valid_genres) > 0:
            return valid_genres

        # Users could not agree on common genres so we return the genres most users agreed on
        return [key for key in genres_map if genres_map[key] >= num_users / 2]

    def post(self, request):
        room_id = request.data["roomId"]
        action = request.data["action"]
        vinder_user = VinderUser.objects.get(user=request.user)
        room = Room.objects.safe_get(room_id=room_id)

        if room is None:
            response_data = {"success": 0, "msg": "No such room"}
            return Response(response_data)

        if room.room_owner != vinder_user:
            response_data = {"success": 0, "msg": "Only room owner can modify room's state"}
            return Response(data=response_data)

        if action == "close":
            room.done = True
            room_users = RoomUser.objects.filter(room=room)
            for room_user in room_users:
                this_vinder_user = room_user.vinder_user
                if this_vinder_user.is_anonymous:
                    associated_user = this_vinder_user.user
                    # No need to delete this_vinder_user, as it will be cascadely deleted by the db
                    associated_user.delete()

        elif action == "start":
            room.started = True
            room_users = RoomUser.objects.filter(room=room)
            valid_genres_names = self.get_valid_genres_names(
                self.create_genres_map(room_users), len(room_users)
            )

            big_q = functools.reduce(
                lambda prev_q, g: prev_q | Q(genre__genre_name=g), valid_genres_names, Q()
            )
            valid_movies = (
                Movie.objects.filter(big_q).order_by("-IMDb_rating").distinct()[:15]
            )
            room_users = RoomUser.objects.filter(room=room)
            for valid_movie in valid_movies:
                room.movie_propositions.add(valid_movie)
                for room_user in room_users:
                    room_user.movies_yet_to_propose.add(valid_movie)

            for valid_genre in valid_genres_names:
                room.matched_genres.add(MovieGenre.objects.get(genre_name=valid_genre))

            room.save()
            for room_user in room_users:
                room_user.save()

        else:
            response_data = {"success": 0, "msg": "Invalid action"}
            return Response(data=response_data)

        room.save()
        response_data = {"success": 1}
        return Response(data=response_data)


class MatchingAlgoAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def check_room_and_user(room, vinder_user):
        if room is None:
            return False, {"success": 0, "msg": "No such room"}

        if room.done or not room.started:
            return False, {
                "success": 0,
                "msg": "Room is already closed or matching is not started yet",
            }

        user_in_room = False
        for room_user in RoomUser.objects.filter(room=room):
            if room_user.vinder_user == vinder_user:
                user_in_room = True

        if not user_in_room:
            return False, {"success": 0, "msg": "User is not in room"}

        return True, None

    @staticmethod
    def get_matched(room_users):
        tmp_list = [room_user.accepted_movies.all() for room_user in room_users]
        return set(i for sub in tmp_list for i in sub if all(i in sub for sub in tmp_list))

    @staticmethod
    def get_declined(room_users):
        tmp_list = [room_user.declined_movies.all() for room_user in room_users]
        return set(i for sub in tmp_list for i in sub if all(i in sub for sub in tmp_list))

    def get_movies_based_on_action(self, action, room):
        room_users = RoomUser.objects.filter(room=room)
        if action == "matched":
            result_set = self.get_matched(room_users)
        elif action == "declined":
            result_set = [room_user.declined_movies.all() for room_user in room_users]
        elif action == "not_matched":
            matched_set = self.get_matched(room_users)
            result_set = set(room.movie_propositions.all()).difference(matched_set)
        else:
            result_set = set()

        result = list(result_set)
        return result

    def get(self, request, room_id):
        vinder_user = VinderUser.objects.get(user=request.user)
        room = Room.objects.safe_get(room_id=room_id)

        is_okay, response_data = self.check_room_and_user(room, vinder_user)
        if not is_okay:
            return Response(response_data)

        action = request.query_params["action"]

        request_result_success = False
        if action == "get_propositions":
            movies_list = room.movie_propositions.all()
            request_result_success = True
        elif action == "get_matched":
            movies_list = self.get_movies_based_on_action("matched", room)
            request_result_success = True
        elif action == "get_declined":
            movies_list = self.get_movies_based_on_action("declined", room)
            request_result_success = True
        elif action == "get_not_matched":
            movies_list = self.get_movies_based_on_action("not_matched", room)
            request_result_success = True
        else:
            movies_list = []

        movie_serializer = MovieSerializer(movies_list, many=True)
        response_data = {
            "success": 1 if request_result_success else 0,
            "action": action[action.find("_") + 1 :]
            if request_result_success
            else "Invalid action",
            "movies": movie_serializer.data,  # [] if request_result_success is False
        }

        return Response(data=response_data)

    def post(self, request, room_id):
        vinder_user = VinderUser.objects.get(user=request.user)
        room = Room.objects.safe_get(room_id=room_id)
        movie_id = request.data["movieId"]
        user_decision = request.data["decision"]

        is_okay, response_data = self.check_room_and_user(room, vinder_user)
        if not is_okay:
            return Response(data=response_data)

        movie = Movie.objects.get(id=movie_id)
        if movie not in room.movie_propositions.all():
            response_data = {"success": 0, "msg": "Movie not in proposed movies"}
            return Response(data=response_data)

        room_user = RoomUser.objects.get(room=room, vinder_user=vinder_user)

        if user_decision == "yes":
            room_user.accepted_movies.add(movie)
        elif user_decision == "no":
            room_user.declined_movies.add(movie)
        else:
            response_data = {"success": 0, "msg": "Invalid decision"}
            return Response(response_data)

        room_user.movies_yet_to_propose.remove(movie)
        room_user.save()
        response_data = {"success": 1}
        return Response(data=response_data)
