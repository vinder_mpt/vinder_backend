from django.db import models
from django.contrib.auth.models import User
import random


class VinderUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_anonymous = models.BooleanField(default=False)


class MovieGenre(models.Model):
    genre_name = models.CharField(max_length=128, unique=True)

    def __str__(self) -> str:
        return self.genre_name


class Movie(models.Model):
    poster_url = models.URLField(null=True, blank=True, max_length=256)
    title = models.CharField(verbose_name="Movie title", max_length=512)
    genre = models.ManyToManyField(MovieGenre)
    director = models.CharField(max_length=256)
    IMDb_rating = models.FloatField()
    description = models.TextField()
    release_year = models.IntegerField()
    duration = models.IntegerField()
    cast = models.TextField()

    def __str__(self) -> str:
        return self.title


class RoomManager(models.Manager):
    def safe_get(self, *args, **kwargs):
        try:
            return self.get(*args, **kwargs)
        except Room.DoesNotExist:
            return None


class Room(models.Model):
    room_id = models.IntegerField(unique=True)
    movie_propositions = models.ManyToManyField(Movie, related_name="movie_propositions")
    matched_movies = models.ManyToManyField(Movie, related_name="matched_movies")
    matched_genres = models.ManyToManyField(MovieGenre, related_name="matched_genres")
    room_owner = models.ForeignKey(
        VinderUser,
        on_delete=models.CASCADE,
        null=True,
        blank=False,
        related_name="room_owner",
    )
    started = models.BooleanField(default=False)
    done = models.BooleanField(default=False)

    objects = RoomManager()

    def __str__(self):
        return f"ID: {self.room_id}"

    @staticmethod
    def generate_room_id():
        tmp_id = random.randint(100000, 999999)
        while True:
            room = Room.objects.safe_get(room_id=tmp_id)
            if room is None:
                break

            tmp_id = random.randint(100000, 999999)

        return tmp_id


class RoomUser(models.Model):
    room = models.ForeignKey(
        Room, on_delete=models.CASCADE, related_name="related_room", null=False
    )
    vinder_user = models.ForeignKey(
        VinderUser, related_name="related_vinder_use", on_delete=models.CASCADE, null=False
    )
    chosen_genres = models.ManyToManyField(MovieGenre)
    accepted_movies = models.ManyToManyField(Movie, related_name="accepted_movies")
    declined_movies = models.ManyToManyField(Movie, related_name="declined_movies")
    movies_yet_to_propose = models.ManyToManyField(
        Movie, related_name="movies_yet_to_propose"
    )

    def __str__(self):
        return f"{self.vinder_user.user.username} | {self.room.room_id}"
