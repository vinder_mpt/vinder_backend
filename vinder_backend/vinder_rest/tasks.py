from celery import shared_task

from .models import Room


@shared_task
def room_cleanup():
    print("Cleaning up rooms...\nDeleting closed rooms")
    closed_rooms = Room.objects.filter(done=True)
    for closed_room in closed_rooms:
        print(f"Deleting room {closed_room}")
        closed_room.delete()

    print("Room cleanup done")
