from django.apps import AppConfig


class VinderRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vinder_rest'
