from django.contrib.auth import get_user_model
from rest_framework import serializers
from .models import MovieGenre, Movie, VinderUser, RoomUser, Room


class MovieGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovieGenre
        fields = ["genre_name"]


class MovieSerializer(serializers.ModelSerializer):
    genre = MovieGenreSerializer(many=True)

    class Meta:
        model = Movie
        fields = "__all__"


class MovieSerializerJustTite(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ["title"]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ["id", "username"]


class VinderUserSerializer(serializers.ModelSerializer):
    user = UserSerializer

    class Meta:
        model = VinderUser
        fields = ["user", "is_anonymous"]


class RoomSerializer(serializers.ModelSerializer):
    movie_propositions = MovieSerializerJustTite(many=True)
    matched_movies = MovieSerializerJustTite(many=True)
    matched_genres = MovieGenreSerializer(many=True)

    class Meta:
        model = Room
        fields = ["room_id", "movie_propositions", "matched_movies", "matched_genres"]


class RoomUserSerializer(serializers.ModelSerializer):
    room = RoomSerializer
    vinder_user = VinderUserSerializer
    chosen_genres = MovieGenreSerializer(many=True)
    accepted_movies = MovieSerializerJustTite(many=True)
    declined_movies = MovieSerializerJustTite(many=True)
    movies_yet_to_propose = MovieSerializerJustTite(many=True)

    class Meta:
        model = RoomUser
        fields = ["room", "vinder_user", "chosen_genres", "accepted_movies", "declined_movies", "movies_yet_to_propose"]
