from django.urls import path
from .views import MovieView, UserAPIView, AnonymousLoginView, MatchingAlgoAPIView, CreateRoomView, \
    JoinRoomView, ModifyRoomStateView

urlpatterns = [
    path("get_movie/<int:pk>/", MovieView.as_view()),
    path("get_user_data/", UserAPIView.as_view()),
    path("anonymous_login/", AnonymousLoginView.as_view()),
    path("match/create_room/", CreateRoomView.as_view()),
    path("match/join_room/", JoinRoomView.as_view()),
    path("match/modify_room/", ModifyRoomStateView.as_view()),
    path("match/room/<int:room_id>/", MatchingAlgoAPIView.as_view()),
]
