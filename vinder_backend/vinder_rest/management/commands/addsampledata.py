from django.core.management.base import BaseCommand
from vinder_rest.models import MovieGenre, Movie


class Command(BaseCommand):
    help = "[TESTING ONLY] Adds sample data to the database using imdb_top_1000.csv file"

    def handle(self, *args, **options):
        movies_in_db = len(Movie.objects.all())
        if movies_in_db > 0:
            self.stdout.write(self.style.SUCCESS("Some sample data already in database, skipping..."))
        else:
            import csv
            tqdm_imported = False
            try:
                from tqdm import tqdm
                tqdm_imported = True
            except ImportError:
                self.stdout.write(self.style.ERROR("tqdm not found. You will not get pretty progress bar."))

            self.stdout.write(self.style.SUCCESS("Adding sample data to database"))
            with open("movies_filtered.csv", "r") as sample_file:
                reader = csv.DictReader(sample_file)

                if tqdm_imported:
                    pbar = tqdm(total=44505)

                movies_list = []
                pk_to_genres_map = {}
                current_pk = 1
                for row in reader:
                    movie = Movie()

                    genres = [
                        MovieGenre.objects.get_or_create(genre_name=genre)[0]
                        for genre in row["genre"].replace(",", "").split()
                    ]

                    pk_to_genres_map[current_pk] = genres
                    current_pk += 1

                    movie.poster_url = row["poster_url"]
                    movie.title = row["title"]
                    movie.release_year = row["year"] if row["year"] != "TV Movie 2019" else 2019
                    movie.director = row["director"]
                    movie.duration = int(row["duration"].split()[0])

                    rating = row["avg_vote"]
                    if rating != "":
                        movie.IMDb_rating = float(rating)
                    else:
                        movie.IMDb_rating = -1.0

                    movie.description = row["description"]
                    movie.cast = row["actors"]
                    movies_list.append(movie)
                    # movie.save()

                    # for genre in genres:
                    #     movie.genre.add(genre)

                    # movie.save()
                    if tqdm_imported:
                        pbar.update(1)

            Movie.objects.bulk_create(movies_list)
            if tqdm_imported:
                pbar.close()

            if tqdm_imported:
                pbar = tqdm(total=44505)
            
            for pk in pk_to_genres_map:
                movie = Movie.objects.get(pk=pk)
                genres = pk_to_genres_map[pk]
                for genre in genres:
                    movie.genre.add(genre)
                
                movie.save()
                if tqdm_imported:
                    pbar.update(1)
            
            if tqdm_imported:
                pbar.close()

            self.stdout.write(
                self.style.SUCCESS("Successfully added sample data to database")
            )
