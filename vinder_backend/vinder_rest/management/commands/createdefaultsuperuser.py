from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from vinder_rest.models import VinderUser


class Command(BaseCommand):
    help = "[TESTING ONLY] Creates default admin/admin superuser. Is executed on startup in docker-compose"

    def handle(self, *args, **options):
        User = get_user_model()
        if len(User.objects.all()) == 0:
            admin = User.objects.create_superuser("admin", "admin@admin.com", "admin")
            vinder_user = VinderUser(user=admin, is_anonymous=False)
            vinder_user.save()
            self.stdout.write(self.style.SUCCESS("Created dev admin user"))
            user = User.objects.create_user(username="Test", email="test@test.com", password="P@ssword123")
            vinder_user2 = VinderUser(user=user, is_anonymous=False)
            vinder_user2.save()
            self.stdout.write(self.style.SUCCESS("Created a regular test user"))
        else:
            self.stdout.write(self.style.SUCCESS("There are users already in the db. Skipping admin creation..."))
