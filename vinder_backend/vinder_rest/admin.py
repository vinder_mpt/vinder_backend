from django.contrib import admin
from .models import MovieGenre, Movie, VinderUser, RoomUser, Room


admin.site.register(Movie)
admin.site.register(MovieGenre)
admin.site.register(VinderUser)
admin.site.register(Room)
admin.site.register(RoomUser)
