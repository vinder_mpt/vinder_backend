import csv
import os
from queue import Empty
import requests
import time
from bs4 import BeautifulSoup
from tqdm import tqdm

from multiprocessing import Process, Queue, Value, freeze_support


def create_new_row(row):
    imdb_base_url = "https://www.imdb.com"
    new_row = {}
    for key, val in row.items():
        new_row[key] = val

    movie_id = row["imdb_title_id"]
    movie_url = imdb_base_url + "/title/" + movie_id + "/"
    response = requests.get(movie_url)

    if response.status_code != 200:
        print(f"STATUS CODE IS NOT 200 FOR {movie_url}")
        print(f"{response.status_code=}")
        new_row["poster_url"] = "N/A"
    else:
        bs = BeautifulSoup(response.text, "html.parser")
        tag = bs.find("a", {"class": "ipc-lockup-overlay ipc-focusable"})
        poster_url = imdb_base_url + tag.attrs.get("href")
        poster_url = poster_url[: poster_url.find("?")]
        response_poster = requests.get(poster_url)
        if response_poster.status_code !=200:
            print(f"STATUS CODE IS NOT 200 FOR {poster_url}")
            print(f"{response_poster.status_code=}")
            new_row["poster_url"] = "N/A"
        else:
            bs1 = BeautifulSoup(response_poster.text, "html.parser")
            poster_div = bs1.find("div", {"class": "iUyzNI"})
            if poster_div is None:
                new_row["poster_url"] = "N/A"
            else:
                img = poster_div.find("img")
                src = img.attrs.get("src")
                new_row["poster_url"] = src

    return new_row


def worker(input, output, done_value: Value):
    for args in iter(input.get, "STOP"):
        output.put(create_new_row(args))
        with done_value.get_lock():
            done_value.value += 1

    # print("!Process finished!")


def doit():
    line_counter = -1

    with open("IMDb_movies.csv", "r", encoding="utf-8") as movies_db:
        for line in movies_db:
            if line != "":
                line_counter += 1

    NUMBER_OF_PROCESSES = 14
    jobs = []
    new_rows_task_results_queue = Queue()
    old_rows_task_queue = Queue()

    todo_list = []

    with open("IMDb_movies.csv", "r", encoding="utf-8") as movies_db:
        reader = csv.DictReader(movies_db)
        writer_fieldnames = reader.fieldnames + ["poster_url"]

        for row in reader:
            todo_list.append(row)
            # old_rows_task_queue.put(row)

    interval = 4000
    total_scheduled = 0
    interval_done = 0

    already_done_chunks = len(
        [filename for filename in os.listdir() if "compiled_movies" in filename]
    )
    num_rows_to_skip = already_done_chunks * (interval + 1)
    while total_scheduled <= line_counter:
        jobs = []
        current = 0
        while current <= interval and len(todo_list) > 0:
            item = todo_list.pop(0)
            current += 1
            total_scheduled += 1
            if total_scheduled > num_rows_to_skip:
                old_rows_task_queue.put(item)

        if total_scheduled > num_rows_to_skip:
            with tqdm(total=current, desc=f"Scraping {interval_done}") as pbar:
                for _ in range(NUMBER_OF_PROCESSES):
                    done_counter = Value("i", 0)
                    proc = Process(
                        target=worker,
                        args=(
                            old_rows_task_queue,
                            new_rows_task_results_queue,
                            done_counter,
                        ),
                    )
                    jobs.append((proc, done_counter))
                    proc.start()
                    old_rows_task_queue.put("STOP")

                done_already = 0
                while not old_rows_task_queue.empty() or done_already < current:
                    this_time_done = 0
                    for job in jobs:
                        job_proc, job_done = job
                        with job_done.get_lock():
                            this_time_done += job_done.value

                    if this_time_done > done_already:
                        pbar.update(this_time_done - done_already)
                        done_already = this_time_done

                    time.sleep(3.5)

            print("Finished data fethings. Starting compiling new csv file.")

            with open(
                f"IMDb_compiled_movies{interval_done}.csv",
                "w",
                encoding="utf-8",
                newline="",
            ) as csvfile:
                writer = csv.DictWriter(csvfile, writer_fieldnames)
                writer.writeheader()

                with tqdm(total=current, desc=f"Compiling {interval_done}") as pbar:
                    while not new_rows_task_results_queue.empty():
                        try:
                            new_row = new_rows_task_results_queue.get(block=False)
                            writer.writerow(new_row)
                            pbar.update(1)
                        except Empty:
                            break

            print("Joining workers")

            for job in jobs:
                job[0].join()

        else:
            print(f"Skipping interval {interval_done}")

        interval_done += 1

    print("All done")


if __name__ == "__main__":
    freeze_support()
    doit()
