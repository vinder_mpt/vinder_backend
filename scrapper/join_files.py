import csv
import os
from tqdm import tqdm


files_to_join = [filename for filename in os.listdir() if "compiled_movies" in filename]

result_filename = "IMDb_movies_compiled.csv"

lines_to_join = []
headers = []

for file in files_to_join:
    with open(file, "r", encoding="utf-8") as f:
        reader = csv.DictReader(f)
        headers = reader.fieldnames
        lines_to_join.extend([row for row in reader])


with open(result_filename, "w", encoding="utf-8", newline="") as res_csv:
    writer = csv.DictWriter(res_csv, headers)
    writer.writeheader()

    for row in tqdm(lines_to_join):
        writer.writerow(row)
