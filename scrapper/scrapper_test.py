import csv
import os
from queue import Empty
import requests
import time
from bs4 import BeautifulSoup
from tqdm import tqdm
from multiprocessing import Process, Queue, Value, freeze_support


def doit():
    url = "https://www.imdb.com/title/tt0170016/"
    response = requests.get(url)
    bs = BeautifulSoup(response.text, "html.parser")
    tag = bs.find("a", {"class": "ipc-lockup-overlay ipc-focusable"})
    poster_url = "https://www.imdb.com" + tag.attrs.get("href")
    poster_url = poster_url[: poster_url.find("?")]
    response_poster = requests.get(poster_url)
    bs1 = BeautifulSoup(response_poster.text, "html.parser")
    div1 = bs1.find("div", {"class": "iUyzNI"})
    img = div1.find("img")
    src = img.attrs.get("src")
    print(src)


if __name__ == "__main__":
    doit()
