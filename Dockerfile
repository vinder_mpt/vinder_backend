FROM python:3.9-alpine

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

COPY requirements.txt /requirements.txt

RUN apk add --update --no-cache postgresql-client

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev

RUN mkdir /app
WORKDIR /app
ADD . /app

RUN adduser -D djangouser && chown -R djangouser /app
USER djangouser

RUN pip install -r /requirements.txt && pip install psycopg2

WORKDIR vinder_backend